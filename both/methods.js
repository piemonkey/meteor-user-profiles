import { check } from 'meteor/check'
import { Accounts } from 'meteor/accounts-base'

const share = __coffeescriptShare

/** Add new email. Check if value not already registered */
share.Methods.userAddEmail = {
  name: 'Accounts.addEmail',
  validate({ userId, address }) {
    check(userId, String)
    check(address, String)
  },
  run({ userId, address }) {
    if (!this.isSimulation) {
      if (Meteor.users.findOne({ _id: userId, 'emails.address': { $in: [address] } })) {
        throw new Meteor.Error(403, `You already have ${address} registered`)
      }
      Meteor.users.update({ _id: userId }, {
        $push: {
          emails: {
            address,
            verified: false,
          },
        },
      })
      Accounts.sendVerificationEmail(userId, address)
    }
  },
}

/** Set email address as primary email */
share.Methods.userMakeEmailPrimary = {
  name: 'Accounts.makePrimary',
  validate({ userId, address }) {
    check(userId, String)
    check(address, String)
  },
  run({ userId, address }) {
    const user = Meteor.users.findOne(userId)

    if (!user) {
      throw new Meteor.Error(403, 'User not found')
    } else {
      const oldEmail = user.emails[0].address
      for (let i = 0; i < user.emails.length; i += 1) {
        if (user.emails[i].address === address) {
          user.emails[i].address = oldEmail
        }
      }
      user.emails[0].address = address
      Meteor.users.update(user._id, {
        $set: {
          emails: user.emails,
        },
      })
    }
  },
}

/** Send email verificatio message */
share.Methods.sendVerificationEmail = {
  name: 'Accounts.sendVerificationEmail',
  validate({ userId, address }) {
    check(userId, String)
    check(address, String)
  },
  run({ userId, address }) {
    if (Meteor.isServer) {
      Accounts.sendVerificationEmail(userId, address)
    }
  },
}

/** Remove email method */
share.Methods.userRemoveEmail = {
  name: 'Accounts.removeEmail',
  validate({ userId, address }) {
    check(userId, String)
    check(address, String)
  },
  run({ userId, address }) {
    Meteor.users.update({ _id: userId }, {
      $pull: {
        emails: { address },
      },
    })
  },
}
