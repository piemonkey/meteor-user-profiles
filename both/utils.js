export const displayName = ({ profile, emails }) =>
  profile.nickname || profile.firstName || `Mx ${profile.lastName}` || emails[0].address
