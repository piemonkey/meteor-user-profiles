import SimpleSchema from 'simpl-schema'


throwError = (error, reason, details) ->
  error = new (Meteor.Error)(error, reason, details)
  if Meteor.isClient
    return error
  else if Meteor.isServer
    throw error
  return

share.Methods = {}

share.Methods.userProfileRemoveUser = {
  name: "Accounts.removeUser"
  validate: (userId) -> check(userId,String)
  run: (userId) -> Meteor.users.remove(userId)
}

share.Methods.userProfileUpdateUser = {
  name: "Accounts.UpdateUser"
  validate: (user) ->
    # XXX this function can update far too much and should be limited to
    # update the profile
    schema = Meteor.users.simpleSchema()
    SimpleSchema.validate(user.modifier,schema,{ modifier: true })
  run: (user) ->
    Meteor.users.update(user._id,user.modifier)
}

share.Methods.userProfileSendEnrollAccount = {
  name: "Accounts.sendEnrollment"
  validate: (userId) -> check(userId,String)
  run: (userId) ->
    if Meteor.isServer
      user = Meteor.users.findOne(userId)
      console.log "Send sendEnrollment Email to #{user.emails[0].address}"
      try
        Accounts.sendEnrollmentEmail(userId)
      catch error
        throwError("500","Internal Server Error while sending Enrollment", error)
}

# TODO . this method should also be trasformed in a validated method.
share.initMethods = (teamName,allRoles) ->

  # Downgrade or upgrade users roles. A user with a role at level n,
  # can downgrade a user (or itself) to a role at level n-1 and upgrade
  # a user to level n
  Meteor.methods 'Accounts.changeUserRole': (id, role) ->
    console.log "Accounts.changeUserRole"
    check(id,String)
    check(role,Match.Where (role) -> role in allRoles)
    user = Meteor.users.findOne(id)
    currentUser = Meteor.userId()
    # a user cannot change his own role
    if (user and !(user._id == currentUser)) ||  Roles.userIsInRole(currentUser, 'manager', teamName)
      if Roles.userIsInRole(currentUser, 'super-admin', teamName)
        if role in ['admin', 'manager', 'user']
          if Meteor.isServer
            console.log "Remove #{user._id} from 'super-admin, admin, manager' for #{teamName}"
            Roles.removeUsersFromRoles(user._id,['super-admin','admin','manager'], teamName)
            console.log "Add #{user._id} to #{role} for #{teamName}"
            Roles.addUsersToRoles(user._id, role, teamName)
            console.log "User Roles #{Roles.getRolesForUser(user._id,teamName)}"
            return
        else
          return throwError(403,
            "Insufficient Permission: Only a super-admin can make a user #{role}")

      else if Roles.userIsInRole(currentUser, 'admin', teamName)
        if role in ['manager', 'user']
          if Meteor.isServer
            console.log "User Roles #{Roles.getRolesForUser(user._id,teamName)}"
            console.log "Remove UserId #{user._id} from Roles 'admin, manager' for #{teamName}"
            # XXX this function does not work !!!
            Roles.removeUsersFromRoles(user._id,['admin','manager'], teamName)
            console.log "Add UserId #{user._id} to Role #{role} for #{teamName}"
            Roles.addUsersToRoles(user._id, role, teamName)
            console.log "User Roles #{Roles.getRolesForUser(user._id,teamName)}"
            return
        else
          return throwError(403,
            "Insufficient Permission: Only an admin can make a user #{role}")

      else if Roles.userIsInRole(currentUser, 'manager', teamName)
        if role in ['user']
          if Meteor.isServer
            console.log "Remove #{user._id} from 'manager' for #{teamName}"
            Roles.removeUsersFromRoles(user._id,['manager'], teamName)
            console.log "Add #{user._id} to #{role} for #{teamName}"
            Roles.addUsersToRoles(user._id, role, teamName)
            return
        else
          return throwError(403,
            "Insufficient Permission: Only a manager can make a user #{role}")

    return throwError(403, "Insufficient Permission")
