
share.ProfilePictures = new FilesCollection
#  debug: true
  storagePath: 'profilePictures'
  collectionName: 'ProfilePictures'
  # permissions: 0774
  # parentDirPermissions: 0774
  allowClientCode: true # Disallow remove files from Client
  onBeforeUpload: (file) ->
    # Allow upload files under 10MB, and only in png/jpg/jpeg formats
    if (file.size <= 10485760 && /png|jpg|jpeg/i.test(file.extension))
      return true
    else
      return i18n.__("abate:meteor-user-profiles","upload_error")
  onBeforeRemove: (cursor) ->
    matches = cursor.fetch()
    return matches.every((file) => file.userId == this.userId)
