import { checkNpmVersions } from 'meteor/tmeasday:check-npm-versions'
checkNpmVersions { 'simpl-schema': '1.x' }, 'abate:meteor-user-profile'
import SimpleSchema from 'simpl-schema'
SimpleSchema.extendOptions(['autoform'])

share.Schemas = {}

# TODO do all of these need to be optional?
share.Schemas.Profile = new SimpleSchema(
  firstName:
    type: String
    label: () -> i18n.__("abate:meteor-user-profiles","firstName")
    optional: true
  lastName:
    type: String
    label: () -> i18n.__("abate:meteor-user-profiles","lastName")
    optional: true
  nickname: {
    type: String,
    optional: true,
  },
  picture:
    type: String
    optional: true
    label: () -> i18n.__("abate:meteor-user-profiles","picture")
    autoform:
      afFieldInput:
        type: 'fileUpload'
        collection: 'ProfilePictures'
  language:
    type: String
    allowedValues: ["en","fr","es"]
    defaultValue: "en"
    optional: true
    autoform:
      afFieldInput:
        type: "select-radio-inline"
        # the icons should be part of this module and not outside
        options: [
          {
            value: "fr",
            label: Spacebars.SafeString(
              '<span class="flag-icon flag-icon-fr"></span>'
              )
          },
          {
            value: "en",
            label: Spacebars.SafeString(
              '<span class="flag-icon flag-icon-gb"></span>'
              )
          },
          {
            value: "es",
            label: Spacebars.SafeString(
              '<span class="flag-icon flag-icon-es"></span>'
              )
          }
        ]
)
