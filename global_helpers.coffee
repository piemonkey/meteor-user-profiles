Template.registerHelper 'imageFileLink', (id) ->
  if id then share.ProfilePictures.findOne(id)?.link()
